package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Select;
import com.example.model.CourseModel;
import com.example.model.StudentModel;

@Mapper
public interface CourseMapper
{
    @Select("select id_course, nama, sks from course where id_course = #{id}")
    @Results(value = {
    	@Result(property="idCourse", column="id_course"),
    	@Result(property="nama", column="nama"),
    	@Result(property="sks", column="sks"),
    	@Result(property="students", column="id_course", 
    			javaType = List.class, 
    			many=@Many(select="selectStudents"))
    })
    CourseModel selectCourse(@Param("id") String id);
    
    @Select("select id_course, nama, sks from course")
    @Results(value = {
    		@Result(property="idCourse", column="id_course"),
        	@Result(property="nama", column="nama"),
        	@Result(property="sks", column="sks"),
    		@Result(property="students", column="id_course", javaType = List.class,
    				many=@Many(select="selectStudents"))
    })
    List<CourseModel> selectAllCourses();
    
    @Select("SELECT student.npm, name, gpa FROM studentcourse JOIN student ON studentcourse.npm = student.npm WHERE studentcourse.id_course = #{id}")
    List<StudentModel> selectStudents(@Param("id") String id);
}
